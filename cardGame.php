<?php


class CardGame
{
    // je n'ai pas trop fait attention à la visibilité des variables
    public $players = [];
    public $cards = [];
    public $cardByPlayer;
    const MAX_CARDS = 52;

    /**
     * cardGame constructor.
     * @param array $players
     */
    public function __construct(array $players)
    {
        foreach ($players as $playerName) {
            $this->players[] = new Player($playerName);
        }
        $this->buildCardsGame();
    }

    private function buildCardsGame()
    {
        for ($i = 1; $i <= self::MAX_CARDS; $i++) {
            $this->cards[] = $i;
        }

        // mixe cards
        $mixedCards = [];
        while (count($this->cards) > 0) {
            $k = rand(0, count($this->cards) - 1);
            $mixedCards[] = array_splice($this->cards, $k, 1)[0];
        }
        $this->cards = $mixedCards;
    }

    private function dispatchCards()
    {
        echo " --- Starting dispating cards ...  \n";
        $this->cardByPlayer = floor(count($this->cards) / count($this->players));
        $continue = true;
        while ($continue) {
            foreach ($this->players as $player) {
                $player->addCard(array_pop($this->cards));
                $continue = count($player->cards) < $this->cardByPlayer;
            }
        }
    }

    public function startPlay()
    {
        echo " --- Let's play :-) \n";
        $this->dispatchCards();
        $resetPattern = ['player' => null, 'level' => 0];
        $higherGame = $resetPattern;

        for ($i = 0; $i < $this->cardByPlayer; $i++) {
            foreach ($this->players as $k => $player) {
                $topCard = $player->getTopCard();
                if ($topCard > $higherGame['level']) {
                    $higherGame = ['player' => $k, 'level' => $topCard];
                }
            }

            $this->players[$higherGame['player']]->addPoint();
            $higherGame = $resetPattern;

        }

        echo " --- end of the game \n";
        $winner = $resetPattern;
        foreach ($this->players as $player) {
            if ($player->getScore() > $winner['level']) {
                $winner = ['player' => $player->getName(), 'level' => $player->getScore()];
            }
        }

        echo " --- the winner is :  " . $winner['player'] . " - " . $winner['level'] . " points ";
    }

}


class Player
{
    public $name;
    public $cards = [];
    public $score = 0;

    /**
     * player constructor.
     * @param $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * return the cards + new one
     *
     * @return array
     */
    public function addCard($card)
    {
        return array_push($this->cards, $card);
    }


    /*
     * return the last card and remove it from the stack
     *
     * @return card
     */
    public function getTopCard()
    {
        return array_pop($this->cards);
    }

    /**
     * @return int
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     *
     */
    public function addPoint()
    {
        $this->score++;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }


}

$players = ['bob', 'ben'];
$game = new CardGame($players);

$game->startPlay();
